package kilaka.kuava;

import static kilaka.kuava.Functional.Runnable.unchecked;

public class AutoClosebleWrapper<C extends AutoCloseable> implements AutoCloseable
{
	private final C wrapped;
	
	public AutoClosebleWrapper(C wrapped)
	{
		this.wrapped = wrapped;
	}
	
	public C getWrapped()
	{
		return wrapped;
	}
	
	@Override
	public void close()
	{
		if (wrapped == null)
		{
			return;
		}
		
		unchecked(wrapped::close)
				.run();
	}
}
