package kilaka.kuava;

import java.io.File;

import static kilaka.kuava.Exceptions.rethrowUnchecked;

public class Functional
{
	@FunctionalInterface
	public interface Function<T, R>
	{
		static <T, R> java.util.function.Function<T, R> unchecked(Function<T, R> f)
		{
			return t -> {
				try
				{
					return f.apply(t);
				}
				catch (Exception e)
				{
					throw rethrowUnchecked(e);
				}
			};
		}
		
		R apply(T t) throws Exception;
	}
	
	
	@FunctionalInterface
	public interface Predicate<T>
	{
		static <T> java.util.function.Predicate<T> unchecked(Predicate<T> p)
		{
			return t -> {
				try
				{
					return p.test(t);
				}
				catch (Exception e)
				{
					throw rethrowUnchecked(e);
				}
			};
		}
		
		boolean test(T t) throws Exception;
	}
	
	@FunctionalInterface
	public interface Consumer<T>
	{
		static <T> java.util.function.Consumer<T> unchecked(Consumer<T> c)
		{
			return t -> {
				try
				{
					c.accept(t);
				}
				catch (Exception e)
				{
					throw rethrowUnchecked(e);
				}
			};
		}
		
		void accept(T t) throws Exception;
	}
	
	@FunctionalInterface
	public interface Supplier<T>
	{
		static <T> java.util.function.Supplier<T> unchecked(Supplier<T> s)
		{
			return () -> {
				try
				{
					return s.get();
				}
				catch (Exception e)
				{
					throw rethrowUnchecked(e);
				}
			};
		}
		
		T get() throws Exception;
	}
	
	@FunctionalInterface
	public interface Runnable
	{
		static java.lang.Runnable unchecked(Runnable r)
		{
			return () -> {
				try
				{
					r.run();
				}
				catch (Exception e)
				{
					throw rethrowUnchecked(e);
				}
			};
		}
		
		void run() throws Exception;
	}
	
	@FunctionalInterface
	public interface FilenameFilter
	{
		static java.io.FilenameFilter unchecked(FilenameFilter ff)
		{
			return (dir, name) -> {
				try
				{
					return ff.accept(dir, name);
				}
				catch (Exception e)
				{
					throw rethrowUnchecked(e);
				}
			};
		}
		
		boolean accept(File dir, String name) throws Exception;
	}
	
	@FunctionalInterface
	public interface IntConsumer
	{
		static java.util.function.IntConsumer unchecked(IntConsumer c)
		{
			return i -> {
				try
				{
					c.accept(i);
				}
				catch (Exception e)
				{
					throw rethrowUnchecked(e);
				}
			};
		}
		
		void accept(int value) throws Exception;
	}
	
	public static <T> java.util.function.Supplier<T> convert(java.lang.Runnable r)
	{
		return () ->
		{
			r.run();
			return null;
		};
	}
}