package kilaka.kuava;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WaitingExecutor
{
	public static final WaitingExecutor NO_OP_WAITING_EXECUTOR = new WaitingExecutor(0);
	
	private static final Logger logger = LoggerFactory.getLogger(WaitingExecutor.class);
	
	private static final ThreadFactory THREAD_FACTORY = r -> {
		Thread thread = new Thread();
		thread.setDaemon(true);
		return thread;
	};
	
	private final int maxConcurrentTasks;
	private final ThreadPoolExecutor executor;
	private final Semaphore currentRunningTasksSemaphore;
	
	public WaitingExecutor(int maxConcurrentTasks)
	{
		this.maxConcurrentTasks = maxConcurrentTasks;
		
		// TODO [kilaka]: Enable plugging a thread factory.
		//  Perhaps even enable plugging the whole thread pool? If so, how to control the queue, etc...
		executor =
				isEnabled()
				? new ThreadPoolExecutor(
						maxConcurrentTasks, maxConcurrentTasks,
						0L, TimeUnit.MILLISECONDS,
						new LinkedBlockingQueue<>(maxConcurrentTasks),
						Executors.defaultThreadFactory())
				: null;
		
		currentRunningTasksSemaphore =
				isEnabled()
				? new Semaphore(maxConcurrentTasks)
				: null;
	}
	
	public boolean isEnabled()
	{
		return (maxConcurrentTasks != 0);
	}
	
	/**
	 * Waits for the tasks to finish.
	 * Running the original if only 1 task can/needs to run.
	 */
	public void submit(Runnable original, Supplier<Collection<Runnable>> splitter)
	{
		if (!(isEnabled()))
		{
			logger.trace("Waiting executor is disabled. Running on the same thread.");
			
			original.run();
			
			return;
		}
		
		Collection<Runnable> tasks = splitter.get();
		
		if (tasks.size() == 1)
		{
			logger.trace("No need to run a single task parallelly. Running on the same thread.");
			
			original.run();
			
			return;
		}
		
		if (!(currentRunningTasksSemaphore.tryAcquire(tasks.size())))
		{
			logger.warn("Not enough resources {}/{} available to run {} tasks concurrently. Running the original task.",
					currentRunningTasksSemaphore.availablePermits(), maxConcurrentTasks, tasks.size());
			
			original.run();
			
			return;
		}
		
		List<Future<?>> futures = null;
		
		try
		{
			futures = new ArrayList<>(tasks.size());
			
			long tasksStartTime = System.currentTimeMillis();
			logger.trace("Concurrent tasks - started - {}.", tasks.size());
			
			for (Runnable task : tasks)
			{
				Future<?> future = executor.submit(() ->
				{
					try
					{
						long taskStartTime = System.currentTimeMillis();
						
						if (logger.isTraceEnabled())
						{
							logger.trace("Concurrent task - #{} - started - {}.", task.hashCode(), task.toString());
						}
						
						task.run();
						
						if (logger.isTraceEnabled())
						{
							logger.trace("Concurrent task - #{} - finished in {}ms.", task.hashCode(), (System.currentTimeMillis() - taskStartTime));
						}
					}
					catch (Exception e)
					{
						logger.error("Concurrent task - #{} - failed.", task.hashCode(), e);
					}
				});
				
				futures.add(future);
			}
			
			for (Future<?> future : futures)
			{
				future.get();
			}
			
			logger.trace("Concurrent tasks - finished - {}ms.", (System.currentTimeMillis() - tasksStartTime));
		}
		catch (Exception e)
		{
			Exceptions.rethrowUnchecked(e);
		}
		finally
		{
			if (futures != null)
			{
				for (Future<?> future : futures)
				{
					future.cancel(true);
				}
			}
			
			currentRunningTasksSemaphore.release(tasks.size());
		}
	}
}
