package kilaka.kuava;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsumerComposite<T> extends ConcurrentLinkedQueue<Consumer<T>> implements Consumer<T>
{
	private static final Logger LOGGER = Logger.getLogger(ConsumerComposite.class.getName());
	
	@Override
	public void accept(T t)
	{
		for (Consumer<T> consumer : this)
		{
			try
			{
				consumer.accept(t);
			}
			catch (Exception e)
			{
				LOGGER.log(Level.SEVERE, "Consumer failed. Continuing to next one (if exists)", e);
			}
		}
	}
}
