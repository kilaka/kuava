package kilaka.kuava;

public class Exceptions
{
	@SuppressWarnings("unchecked")
	// Source: https://stackoverflow.com/a/19757456/435605
	public static <T extends Throwable> RuntimeException rethrowUnchecked(Throwable throwable) throws T
	{
		throw (T) throwable; // rely on vacuous cast
	}
}
