package kilaka.kuava.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import kilaka.kuava.WaitingExecutor;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class WaitingExecutorTest
{
	@Test
	public void testSimpleScenario()
	{
		int n = 11;
		int chunks = 4;
		
		AtomicLong res = new AtomicLong(1);
		
		WaitingExecutor waitingExecutor = new WaitingExecutor(100);
		
		waitingExecutor.submit(
				() -> res.set(factorial(1, n)),
				() ->
				{
					int jump = (int) Math.ceil(n / (double) chunks);
					List<Runnable> tasks = new ArrayList<>(chunks);
					for (int i = 0; i < chunks; i++)
					{
						final int fi = i;
						tasks.add(new Runnable()
						{
							private final int from = fi * jump + 1;
							private final int to = Math.min((fi + 1) * jump, n);
							
							@Override
							public void run()
							{
								long resLocal = factorial(from, to);
								res.accumulateAndGet(resLocal, (x, y) -> x * y);
							}
							
							@Override
							public String toString()
							{
								return "factorial{" +
								       "from=" + from +
								       ", to=" + to +
								       '}';
							}
						});
					}
					return tasks;
				});
		
		Assertions.assertThat(res.get()).isEqualTo(factorial(1, n));
	}
	
	@Test
	public void testNotEnoughResources()
	{
		int n = 10;
		
		AtomicLong res = new AtomicLong(1);
		
		WaitingExecutor waitingExecutor = new WaitingExecutor(2);
		
		waitingExecutor.submit(
				() -> res.set(factorial(1, n)),
				() -> Arrays.asList(null, null, null));
		
		Assertions.assertThat(res.get()).isEqualTo(factorial(1, n));
	}
	
	@Test
	public void testSingleTaskRunsOriginal()
	{
		int n = 10;
		
		AtomicLong res = new AtomicLong(1);
		
		WaitingExecutor waitingExecutor = new WaitingExecutor(2);
		
		waitingExecutor.submit(
				() -> res.set(factorial(1, n)),
				() -> Collections.singletonList(() -> {}));
		
		Assertions.assertThat(res.get()).isEqualTo(factorial(1, n));
	}
	
	private long factorial(int from, int to)
	{
		long res = 1;
		
		for (int i = from; i <= to; i++)
		{
			res *= i;
		}
		
		return res;
	}
}
