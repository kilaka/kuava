package kilaka.kuava.test;

import kilaka.kuava.Exceptions;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class RethrowUncheckedTest {

	private static void throwAsUnchecked() {
		try {
			throw new MyException();
		} catch (Exception e) {
			throw Exceptions.rethrowUnchecked(e);
		}
	}

	@Test
	public void testExceptionNotDeclared() {
		try {
			throwAsUnchecked();
		} catch (Exception e) {
			Assertions.assertThat(e).isInstanceOf(MyException.class);
			Assertions.assertThat(e.getMessage()).isEqualTo(MyException.EXCEPTION_TEXT);

		}
	}
}
