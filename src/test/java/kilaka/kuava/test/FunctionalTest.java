package kilaka.kuava.test;

import kilaka.kuava.Functional;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.File;
import java.io.FilenameFilter;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.IntConsumer;

public class FunctionalTest
{
	
	private static final AtomicReference<Object> TRASH = new AtomicReference<>(0);
	
	@Test
	public void testUsingCheckedExceptionWithLambdas()
	{
		invoke(toRunnable(Functional.Function.unchecked(this::functionThatDeclaresException)));
		invoke(toRunnable(Functional.Predicate.unchecked(this::predicateThatDeclaresException)));
		invoke(toRunnable(Functional.Supplier.unchecked(this::supplierThatDeclaresException)));
		invoke(toRunnable(Functional.Consumer.unchecked(this::consumerThatDeclaresException)));
		invoke(toRunnable(Functional.FilenameFilter.unchecked(this::filenameFilterThatDeclaresException)));
		invoke(toRunnable(Functional.IntConsumer.unchecked(this::intConsumerThatDeclaresException)));
		invoke(Functional.Runnable.unchecked(this::runnableThatDeclaresException));
	}
	
	private void invoke(Runnable runnable)
	{
		Assertions.assertThatThrownBy(runnable::run).isInstanceOf(MyException.class);
	}
	
	private void intConsumerThatDeclaresException(int i) throws MyException
	{
		functionThatDeclaresException(i);
	}
	
	private Boolean functionThatDeclaresException(Integer i) throws MyException
	{
		TRASH.set(i); // Do something with i in order to not see it in warnings.
		throw new MyException();
	}
	
	private boolean filenameFilterThatDeclaresException(File dir, String name) throws MyException
	{
		// Do something with params in order to not see it in warnings.
		TRASH.set(dir);
		TRASH.set(name);
		
		throw new MyException();
	}
	
	private Boolean supplierThatDeclaresException() throws MyException
	{
		return predicateThatDeclaresException(1);
	}
	
	private boolean predicateThatDeclaresException(Integer i) throws MyException
	{
		functionThatDeclaresException(i);
		return true;
	}
	
	private void consumerThatDeclaresException(Integer i) throws MyException
	{
		functionThatDeclaresException(i);
	}
	
	private void runnableThatDeclaresException() throws MyException
	{
		functionThatDeclaresException(1);
	}
	
	public static <R> java.lang.Runnable toRunnable(java.util.function.Supplier<R> supplier)
	{
		return supplier::get;
	}
	
	public static <T> java.lang.Runnable toRunnable(java.util.function.Predicate<T> predicate)
	{
		return () -> predicate.test(null);
	}
	
	public static <T> java.lang.Runnable toRunnable(java.util.function.Consumer<T> consumer)
	{
		return () -> consumer.accept(null);
	}
	
	public static java.lang.Runnable toRunnable(java.util.function.Function<?, ?> function)
	{
		return () -> function.apply(null);
	}
	
	private Runnable toRunnable(FilenameFilter filenameFilter)
	{
		return () -> filenameFilter.accept(null, null);
	}
	
	private Runnable toRunnable(IntConsumer intConsumer)
	{
		return () -> intConsumer.accept(-1);
	}
}
