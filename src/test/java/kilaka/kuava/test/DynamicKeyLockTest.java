package kilaka.kuava.test;

import java.util.concurrent.atomic.AtomicBoolean;

import kilaka.kuava.DynamicKeyLock;
import org.junit.Assert;
import org.junit.Test;

public class DynamicKeyLockTest
{
	@Test
	public void testDifferentKeysDontLock() throws InterruptedException
	{
		DynamicKeyLock<Object> lock = new DynamicKeyLock<>(new Object());
		lock.lock();
		AtomicBoolean anotherThreadWasExecuted = new AtomicBoolean(false);
		try
		{
			new Thread(() ->
			{
				DynamicKeyLock<Object> anotherLock = new DynamicKeyLock<>(new Object());
				anotherLock.lock();
				try
				{
					anotherThreadWasExecuted.set(true);
				}
				finally
				{
					anotherLock.unlock();
				}
			}).start();
			Thread.sleep(100);
		}
		finally
		{
			Assert.assertTrue(anotherThreadWasExecuted.get());
			lock.unlock();
		}
	}
	
	@Test
	public void testSameKeysLock() throws InterruptedException
	{
		Object key = new Object();
		DynamicKeyLock<Object> lock = new DynamicKeyLock<>(key);
		lock.lock();
		AtomicBoolean anotherThreadWasExecuted = new AtomicBoolean(false);
		try
		{
			new Thread(() ->
			{
				DynamicKeyLock<Object> anotherLock = new DynamicKeyLock<>(key);
				anotherLock.lock();
				try
				{
					anotherThreadWasExecuted.set(true);
				}
				finally
				{
					anotherLock.unlock();
				}
			}).start();
			Thread.sleep(100);
		}
		finally
		{
			Assert.assertFalse(anotherThreadWasExecuted.get());
			lock.unlock();
		}
	}
}
